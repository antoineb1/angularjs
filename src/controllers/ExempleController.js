app.controller('ExempleController',function($scope) {
	var name = "test";
	$scope.name = "";
	$scope.age = "";

	$scope.messages = [{
		name: 'Bernard',
		age: 18
	},{
		name: 'Julie',
		age: 18
	},{
		name: 'Beber',
		age: 18
	}];


	$scope.remove = function(j) {
		$scope.messages.splice(j, 1);
	}
	$scope.f = function() {
		return $scope.messages.push({
			name: 'Beber',
			age: 18
		});
	}
	$scope.add = function() {
		$scope.messages.push({
			name: $scope.name,
			age: $scope.age
		});

		$scope.name = "";
		$scope.age = "";
	}


});