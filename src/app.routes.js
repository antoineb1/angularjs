app.config(function($routeProvider, $locationProvider) {
	$routeProvider
		.when('/', {
			controller: 'ExempleController', 
			templateUrl: 'src/views/exemple.html'
		})
		.otherwise({redirectTo : '/'})
});